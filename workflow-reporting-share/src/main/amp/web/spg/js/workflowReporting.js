/**
 * SPG root namespace.
 * 
 * @namespace SPG
 */
if (typeof SPG == "undefined" || !SPG) {
    var SPG = {};
    /* global var to Define panel handlers */
    var parent = {};
}
/* check if a list contains an element */
function contains(a, obj) {
    for (var i = 0; i < a.length; i++) {
        if (a[i] === obj) {
            return true;
        }
    }
    return false;
}
/**
 * SPG dashlet namespace.
 * 
 * @namespace SPG.dashlet
 */
if (typeof SPG.dashlet == "undefined" || !SPG.dashlet) {
    SPG.dashlet = {};
}
/**
 *  dashboard Workflow reporting component.
 * 
 * @namespace SPG.dashlet
 * @class SPG.dashlet.Reporting
 * @Seifallh Bellassoued
 */
(function () {
    /**
     * YUI Library aliases
     */
    var Dom = YAHOO.util.Dom
        , Event = YAHOO.util.Event;

    /**
     * Dashboard Reporting constructor.
     * 
     * @param {String}
     *            htmlId The HTML id of the parent element
     * @return {SPG.dashlet.Reporting} The new component instance
     * @constructor
     */
    SPG.dashlet.Reporting = function Reporting_constructor(htmlId) {
        /* Define panel handlers */
        parent = this;
        SPG.dashlet.Reporting.superclass.constructor.call(this, "SPG.dashlet.Reporting", htmlId);
        return this;
    };
    /**
     * Extend from Alfresco.component.Base and add class implementation
     */
    YAHOO.extend(SPG.dashlet.Reporting, Alfresco.component.Base, {
        /**
         * Object container for initialization options
         * 
         * @property options
         * @type object
         */
        options: {
            /**
             *check if start date is valid
             *@type boolean
             */
            startDateValidity: true
            , /**
             *check if end date is valid
             *@type boolean
             */
            endDateValidity: true
            , /**
             *list of workflow definition names
             *@type array
             */
            arrayWorkflowDefinitions: []
            , /**
             *list of workflow definition ids indexed by workflow names
             *@type array
             */
            arrayWorkflowDefinitionsIds: []
            ,/**
             *list of all workflow instances
             *@type array
             */
            list_of_all_workflow: new Array()
            , /**
             *number of workflows instances in progress of selected workflow definition
             *@type integer
             */
            encours_counter: 0
            , /**
             *number of workflows instances finished of selected workflow definition
             *@type integer
             */
            termine_counter: 0
            , /**
             *number of workflows instances overdue of selected workflow definition
             *@type integer
             */
            retard_counter: 0
            , /**
             *liste of coordinates of points in finished curve (x:date, y:number of finished workflows)
             *@type array
             */
            Termine_per_Day_number: []
            , /**
             *liste of coordinates of points of curve  in progress (x:date, y:number of in progress workflows)
             *@type array
             */
            Encours_per_Day_number: []
            , /**
             *liste of coordinates of points of curve of overdue (x:date, y:number of overdue workflows)
             *@type array
             */
            Retard_per_Day_number: []
            , /**
             *liste of overdue instances of selected workflow definition indexed by date
             *@type array
             */
            RetardList_per_Day: []
            , /**
             *liste of in progress instances of selected workflow definition indexed by date
             *@type array
             */
            encoursList_per_Day: []
            , /**
             *liste of finished instances of selected workflow definition indexed by date
             *@type array
             */
            termineList_per_Day: []
            ,/**
             *selected workflow definition
             *@type String
             */
            workflow_definition : ""
            ,/**
             *selected start date
             *@type String
             */
            start_date: ""
            ,/**
             *selected end date
             *@type String
             */
            end_date: ""
          }
        , /**
         * Fired by YUI when parent element is available for scripting
         * 
         * @method onReady
         */
        onReady: function Reporting_onReady() {
            // setup workflow input event
            Event.addListener(this.id + "-myInput", "keyup", this.handleFieldChange, this, true);
            //autocomplete workflow name 
            this.autocomplete();
        }
        , /**
         * Handles the workfow name being changed in input field.
         * @method _handleFieldChange
         * @param event
         */
        handleFieldChange: function myInput__handleFieldChange(event) {
            var changedWorkflow = Dom.get(parent.id + "-myInput").value;
            if (changedWorkflow.length > 0) {
                //check if the changedWorkflow exist in the workflow names list
                if (contains(parent.options.arrayWorkflowDefinitions, changedWorkflow)) {
                    Dom.removeClass(parent.id + "-myInput", "invalidInput");
                    Dom.addClass(parent.id + "-alf-id2", "yui-overlay");
               }
                else {
                    //check if the changedWorkflow is a beginning of an existing wokflow name
                    var i = 0;
                    while (i < parent.options.arrayWorkflowDefinitions.length) {
                        if (parent.options.arrayWorkflowDefinitions[i].toUpperCase().startsWith(changedWorkflow.toUpperCase())) {
                            Dom.removeClass(parent.id + "-myInput", "invalidInput");
                            Dom.addClass(parent.id + "-alf-id2", "yui-overlay");
                            break;
                        }
                        i++;
                    }
                    if (i >= parent.options.arrayWorkflowDefinitions.length) {
                        Dom.addClass(parent.id + "-myInput", "invalidInput");
                        Dom.removeClass(parent.id + "-alf-id2", "yui-overlay");
                        Dom.setXY(parent.id + "-alf-id2", [Dom.getX(parent.id + "-myInput"), Dom.getY(parent.id + "-myInput") - 44]);
                    }
                    //onfocus of input of workflow name 
                    Dom.get(parent.id + "-myInput").onfocus = function () {
                        if (contains(Dom.get(parent.id + "-myInput").classList, "invalidInput")) Dom.removeClass(parent.id + "-alf-id2", "yui-overlay");
                        var test = false;
                        for (i = 0; i < parent.options.arrayWorkflowDefinitions.length; i++) {
                            if (parent.options.arrayWorkflowDefinitions[i].toUpperCase().startsWith(changedWorkflow.toUpperCase())) {
                                test = true;
                                break;
                            }
                            else test = false
                        }
                        if (test && Dom.get(parent.id + "-myInput").value.length > 0) {
                            Dom.removeClass(parent.id + "-myInput", "invalidInput");
                            Dom.addClass(parent.id + "-alf-id2", "yui-overlay");
                        }
                        else if (!test && Dom.get(parent.id + "-myInput").value.length > 0) {
                            Dom.addClass(parent.id + "-myInput", "invalidInput");
                            Dom.removeClass(parent.id + "-alf-id2", "yui-overlay");
                        }
                    };
                    //onblur of input of workflow name 
                    Dom.get(parent.id + "-myInput").onblur = function () {
                        if (contains(Dom.get(parent.id + "-myInput").classList, "invalidInput")) Dom.addClass(parent.id + "-alf-id2", "yui-overlay");
                        if (!contains(parent.options.arrayWorkflowDefinitions, Dom.get(parent.id + "-myInput").value) && Dom.get(parent.id + "-myInput").value.length > 0) {
                            Dom.addClass(parent.id + "-myInput", "invalidInput");
                        }
                    };
                }
            }
            //if input is empty remove failure message and color
            else {
                Dom.removeClass(parent.id + "-myInput", "invalidInput");
                Dom.addClass(parent.id + "-alf-id2", "yui-overlay");
            }
        }
        , /**
         * make jquery canvas chart.
         * @method _makeChart
         */
        makeChart: function _makeChart() {
            parent.options.workflow_definition = Dom.get(parent.id + "-myInput").value;
            parent.options.start_date = Dom.get(parent.id + "-startd").value;
            parent.options.end_date = Dom.get(parent.id + "2-startd").value;
            //get selected dates
            start_date = new Date(Dom.get(parent.id + "-startd").value);
            end_date = new Date(Dom.get(parent.id + "2-startd").value);
            j = 0;
            parent.options.Termine_per_Day_number = [];
            parent.options.Encours_per_Day_number = [];
            parent.options.Retard_per_Day_number = [];
            parent.options.RetardList_per_Day = [];
            parent.options.encoursList_per_Day = [];
            parent.options.termineList_per_Day = [];
            //browse choosed period per day 
            for (var date_counter = start_date; date_counter <= end_date; date_counter.setDate(date_counter.getDate() + 1)) {
                parent.options.termine_counter = 0;
                parent.options.encours_counter = 0;
                parent.options.retard_counter = 0;
                RetardElement = [];
                TermineElement = [];
                EncoursElement = [];
                var localDate = new Date(date_counter);
                //browse workflow instances list of selected workflow definition
                for (var i = 0; i < parent.options.list_of_all_workflow.length; i++) {
                    //get workflow starting date
                    var dStart = new Date(parent.options.list_of_all_workflow[i].startDate);
                    dStart.setHours(0, 0, 0, 0);
                    //get workflow ending date
                    var dEnd = new Date(parent.options.list_of_all_workflow[i].endDate);
                    dEnd.setHours(0, 0, 0, 0);
                    //get workflow due date
                    var dDue = new Date(parent.options.list_of_all_workflow[i].dueDate);
                    date_counter.setHours(0, 0, 0, 0);
                    //fill each list (EncoursElement, RetardElement)
                    //check if the current workflow instance is started at or before counter date
                    if (dStart.getTime() <= date_counter.getTime()) {
                        //add workflow instance to in progress list if his ending date is null
                        if (parent.options.list_of_all_workflow[i].endDate == null) {
                            parent.options.encours_counter++;
                            EncoursElement.push(parent.options.list_of_all_workflow[i]);
                        }
                        //add workflow instance to overdue list if his ending date is null and his due date exceeds counter date
                        if (parent.options.list_of_all_workflow[i].endDate == null && parent.options.list_of_all_workflow[i].dueDate != null && dDue.getTime() < date_counter.getTime()) {
                            parent.options.retard_counter++;
                            RetardElement.push(parent.options.list_of_all_workflow[i]);
                        }
                        //add workflow instance to finished list if his ending date is before counter date
                        if (parent.options.list_of_all_workflow[i].endDate != null && dEnd.getTime() <= date_counter.getTime()) {
                            parent.options.termine_counter++;
                            TermineElement.push(parent.options.list_of_all_workflow[i]);
                        }
                        //add workflow instance to in rogress list if his ending date is after counter date
                        if (dEnd.getTime() > date_counter.getTime()) {
                            parent.options.encours_counter++;
                            EncoursElement.push(parent.options.list_of_all_workflow[i]);
                        }
                        //add workflow instance to overdue list if his ending date is before counter date and his due date is before counter date 
                        if (dEnd.getTime() > date_counter.getTime() && parent.options.list_of_all_workflow[i].dueDate != null && dDue.getTime() < date_counter.getTime()) {
                            parent.options.retard_counter++;
                            RetardElement.push(parent.options.list_of_all_workflow[i]);
                        }
                    }
                };
                //make overdue list ready to be shown in the popup when date is clicked
                parent.options.RetardList_per_Day.push({
                    array: RetardElement
                    , date: localDate
                });
                //make in pogress list ready to be shown in the popup when date is clicked
                parent.options.encoursList_per_Day.push({
                    array: EncoursElement
                    , date: localDate
                });
                //make finished list ready to be shown in the popup when date is clicked
                parent.options.termineList_per_Day.push({
                    array: TermineElement
                    , date: localDate
                });
                //make finished list ready for curve
                parent.options.Termine_per_Day_number[j] = {
                    x: localDate
                    , y: parent.options.termine_counter
                    //, click: parent.onClick
                };
                //make in pogress list ready for curve
                parent.options.Encours_per_Day_number[j] = {
                    x: localDate
                    , y: parent.options.encours_counter
                    //, click: parent.onClick
                };
                //make overdue list ready for curve
                parent.options.Retard_per_Day_number[j] = {
                    x: localDate
                    , y: parent.options.retard_counter
                    //, click: parent.onClick
                };
                j++;
            };
            
            //drow graphs

            var selected_days = [];
            var selected_days_popup = [];
            for (var i = 0; i < parent.options.Retard_per_Day_number.length; i++) {
                selected_days.push(parent.options.Retard_per_Day_number[i].x.getDate()+ " " + parent.options.Retard_per_Day_number[i].x.toLocaleString(parent.msg("DateZone"), { month: "short" }));
                selected_days_popup.push(parent.options.Retard_per_Day_number[i].x);
            }
            var Retard_per_Day_number_values = [];
            for (var i = 0; i < parent.options.Retard_per_Day_number.length; i++) {
                Retard_per_Day_number_values.push(parent.options.Retard_per_Day_number[i].y);
            }
            var Encours_per_Day_number_values = [];
            for (var i = 0; i < parent.options.Retard_per_Day_number.length; i++) {
                Encours_per_Day_number_values.push(parent.options.Encours_per_Day_number[i].y);
            }
            var Termine_per_Day_number_values = [];
            for (var i = 0; i < parent.options.Retard_per_Day_number.length; i++) {
                Termine_per_Day_number_values.push(parent.options.Termine_per_Day_number[i].y);
            }
            
            var config = {
                type: 'line',
                data: {
                    labels: selected_days,
                    datasets: [{
                        label: parent.msg("late"),
                        data: Retard_per_Day_number_values,
                        fill: false,
                       backgroundColor: "#E9573f",
                       borderColor : "#E9573f",
                        pointBorderColor : "#E9573f",
                        pointBackgroundColor : "#E9573f",
                        pointBorderWidth : 1,
                    //borderDash: [20, 5],
                    pointRadius: 3,
                    pointHoverRadius: 6,
                },{
                        label: parent.msg("inprogress"),
                        data: Encours_per_Day_number_values,
                        backgroundColor: "#4fc1e9",
                        borderColor : "#4fc1e9",
                        pointBorderColor : "#4fc1e9",
                        pointBackgroundColor : "#4fc1e9",
                        pointBorderWidth : 1,
                        fill: false,
                    //borderDash: [20, 5],
                    pointRadius: 3,
                    pointHoverRadius: 6,
                },{
                        label: parent.msg("finished"),
                        backgroundColor: "#A0D468",
                        borderColor : "#A0D468",
                        pointBorderColor : "#A0D468",
                        pointBackgroundColor : "#A0D468",
                        pointBorderWidth : 1,
                        data: Termine_per_Day_number_values,
                        fill: false,
                    //borderDash: [20, 5],
                    pointRadius: 3,
                    pointHoverRadius: 6,
                }]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'bottom'
                },
                hover: {
                    mode: 'label'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            //labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: parent.msg("workflow.number") 
                        }
                    }]
                },
                title: {
                    display: true,
                    text: parent.msg("graph.msg1") + " " + Dom.get(parent.id + "-myInput").value + " " + parent.msg("graph.msg2"),
                    padding: 20,
                    
                }
            }
        };
        var config2 = {
            type: 'bar',
            data: {
                labels: selected_days,
                datasets: [{
                        label: parent.msg("late"),
                        data: Retard_per_Day_number_values,
                        fill: false,
                        backgroundColor: "#E9573f",
                        
                    //borderDash: [20, 5],
                    pointRadius: 5,
                    pointHoverRadius: 10,
                },{
                         label: parent.msg("inprogress"),
                        data: Encours_per_Day_number_values,
                        fill: false,
                        backgroundColor: "#4fc1e9",
                    //borderDash: [20, 5],
                    pointRadius: 5,
                    pointHoverRadius: 10,
                },{
                        label: parent.msg("finished"),
                        data: Termine_per_Day_number_values,
                        fill: false,
                    //borderDash: [20, 5],
                    pointRadius: 5,
                    backgroundColor: "#A0D468",
                    pointHoverRadius: 10,
                }]

                
            },
            options: {
                responsive: true,
                legend: {
                    position: 'bottom',
                },
                hover: {
                    mode: 'label'
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        display: true,
                        scaleLabel: {
                            display: true,
                            //labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        stacked: true,  
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: parent.msg("workflow.number") 
                        }
                    }]
                },
                title: {
                    display: true,
                    text: parent.msg("graph.msg1") + " " + Dom.get(parent.id + "-myInput").value + " " + parent.msg("graph.msg2"),
                     padding: 20
                }
            }
        };
            
        var ctx1, ctx2;
        var resetCanvas = function(){
            var myLine1, myBar;
            
            $('#canvas').remove(); // this is my <canvas> element
            $('#canvas2').remove(); // this is my <canvas> element
            $('#tabs-1').append('<canvas id="canvas" style="position: relative"></canvas>');
            $('#tabs-2').append('<canvas id="canvas2" style="position: relative"></canvas>');
            ctx1 = Dom.get("canvas").getContext("2d");
            ctx2 = Dom.get("canvas2").getContext("2d");
            ctx1.fillText('Space to add something here with 50x100 padding',5,20);
            var myLine1 = new Chart(ctx1, config);
            var myBar = new Chart(ctx2, config2);
            
            canvas2.onclick = function(evt){
                var activePoints = myBar.getElementsAtEvent(evt);
                 if (activePoints > []) {parent.onClick(selected_days_popup[activePoints[0]._index])}
                    
            };
            canvas.onclick = function(evt){
                var activePoints = myLine1.getElementsAtEvent(evt);
                 if (activePoints > []) {parent.onClick(selected_days_popup[activePoints[0]._index])}
            };
        };
        //export graph as png
        function Ga(a, c, b) {
            if (a && c && b) {
                b = b + "." + c;
                var d = "image/" + c;
                a = a.toDataURL(d);
                var e = !1
                , f = document.createElement("a");
                f.download = b;
                f.href = a;
                f.target = "_blank";
                if ("undefined" !== typeof Blob && new Blob) {
                    for (var g = a.replace(/^data:[a-z/]*;base64,/, ""), g = atob(g), h = new ArrayBuffer(g.length), h = new Uint8Array(h), p = 0; p < g.length; p++) h[p] = g.charCodeAt(p);
                        c = new Blob([h.buffer], {
                            type: "image/" + c
                        });
                    try {
                        window.navigator.msSaveBlob(c, b), e = !0
                    }
                    catch (k) {
                        f.dataset.downloadurl = [d, f.download
                        , f.href].join(":"), f.href = window.URL.createObjectURL(c)
                    }
                }
                if (!e) try {
                    event = document.createEvent("MouseEvents"), event.initMouseEvent("click", !0, !1, window, 0, 0, 0, 0, 0, !1, !1, !1, !1, 0, null), f.dispatchEvent ? f.dispatchEvent(event) : f.fireEvent && f.fireEvent("onclick")
                }
                catch (n) {
                    c = window.open(), c.document.write("<img src='" + a + "'></img><div>Please right click on the image and save it to your device</div>"), c.document.close()
                }
            }
        }
        resetCanvas();
        Dom.get('exportButton').onclick = function(){
                   if($( "#tabs" ).tabs( "option", "active" )==1)
                   { var url=Dom.get("canvas2");
                   // document.getElementById("images").src=url;
                   var w = url.width;
                   var h = url.height;
                   
                   var data;   
                   data = ctx2.getImageData(0, 0, w, h);

                    //store the current globalCompositeOperation
                    var compositeOperation = ctx2.globalCompositeOperation;

                    //set to draw behind current content
                    ctx2.globalCompositeOperation = "destination-over";

                    //set background color
                    ctx2.fillStyle = "rgb(255, 255, 255)";

                    //draw background / rect on entire canvas
                    ctx2.fillRect(0,0,w,h);
                   
                   
                start_date = parent.options.start_date.replace(/-/g,"");
                end_date = parent.options.end_date.replace(/-/g,"");
                var str = parent.options.workflow_definition  + "-G2-" + start_date.toString() + "-" +end_date.toString();
                //str = str.replace(" ","_");
                Ga(url, "png", str);
                ctx2.globalCompositeOperation = compositeOperation;
              //  console.log(Ga(url, "jpeg", "ddd"));
            }else if($( "#tabs" ).tabs( "option", "active" )==0){
                 var url=Dom.get("canvas");
                   // document.getElementById("images").src=url;
                   var w = url.width;
                   var h = url.height;
                   
                   var data;   
                   data = ctx1.getImageData(0, 0, w, h);

                    //store the current globalCompositeOperation
                    var compositeOperation = ctx2.globalCompositeOperation;

                    //set to draw behind current content
                    ctx1.globalCompositeOperation = "destination-over";

                    //set background color
                    ctx1.fillStyle = "rgb(255, 255, 255)";

                    //draw background / rect on entire canvas
                    ctx1.fillRect(0,0,w,h);
                   
                    start_date = parent.options.start_date.replace(/-/g,"");
                end_date = parent.options.end_date.replace(/-/g,"");
                var str = parent.options.workflow_definition  + "-G1-" + start_date + "-" +end_date;
                //console.log(end_date);
                //console.log(start_date);
                //str = str.replace(" ","_");
                Ga(url, "png", str);
                ctx1.globalCompositeOperation = compositeOperation;
            }
        };
        //Better to construct options first and then pass it as a parameter
 
           //remove hidden class from charts container
           Dom.removeClass("tabs", "hidden");
        $("#tabs").tabs({
            create: function (event, ui) {
                resetCanvas();
            //Render Charts after tabs have been created.          
            },
            activate: function (event, ui) {
                //Updates the chart to its container's size if it has changed.
                resetCanvas();
                //console.log(ui.newPanel.children().first());
            
            }
        });

        $(function() {
            $( "#tabs" ).tabs({
                active: 1
            });
        });
           $(function() {
             $( "#tabs" ).tabs({
                active: 0
            });
        });
       }
        , /**
         * Show popup that contains workflow instances in the clicked day.
         * @method _onClick
         * @param e (curve clicked point)
         */
        onClick: function _onClick(the_Day) {
            //get clicked curve point coordinates
       
            var x = new Date(the_Day);
            //parent.options.Retard_per_Day_number[i].x.getDate()+ " " + parent.options.Retard_per_Day_number[i].x.toLocaleString(parent.msg("DateZone"), { month: "short" })
            //var y = e.dataPoint.y;
            //div1 = "";
            //div2 = "";
            // div3 = "";
            var late_counter = 0;
            var progress_counter = 0;
            var finished_counter = 0;
            var workflows_click_array = [];
            //pass clicked date to popup titlePOPUP
            var popupDate = x.toLocaleDateString(parent.msg("DateZone"));
            Dom.get(parent.id + "-titlePOPUP").innerHTML = parent.msg("popup.title") + " "+  popupDate;
            //prepare overdue workflow list to popup
            for (var i = 0; i < parent.options.RetardList_per_Day.length; i++) {
                if (parent.options.RetardList_per_Day[i].array.length > 0) {
                    for (var j = 0; j < parent.options.RetardList_per_Day[i].array.length; j++) {
                        if (parent.options.RetardList_per_Day[i].date.getTime() == x.getTime()) {
                            //var lien1 = 'window.open("' + parent.options.domaina + '/share/page/workflow-details?workflowId=' + parent.options.RetardList_per_Day[i].array[j].id + '")';
                            //"<li onclick=" + lien1 + ">" + parent.options.RetardList_per_Day[i].array[j].message + "</li>";
                            if (!workflows_click_array[late_counter]) {
                                workflows_click_array[late_counter] = {
                                    late: ""
                                   // , late_id: ""
                                    , progress: ""
                                   // , progress_id: ""
                                    , finished: ""
                                   // , finished_id: ""
                                };
                                workflows_click_array[late_counter].late = parent.options.RetardList_per_Day[i].array[j].message + "/" + parent.options.RetardList_per_Day[i].array[j].id;
                               // workflows_click_array[late_counter].late_id = parent.options.RetardList_per_Day[i].array[j].id;
                                late_counter++;
                            }
                            else {
                                workflows_click_array[late_counter].late = parent.options.RetardList_per_Day[i].array[j].message + "/" + parent.options.RetardList_per_Day[i].array[j].id;
                                //workflows_click_array[late_counter].late_id = parent.options.RetardList_per_Day[i].array[j].id;
                                late_counter++;
                            }
                        }
                    }
                }
                if (parent.options.RetardList_per_Day[i].array.length <= 0 && parent.options.RetardList_per_Day[i].date.getTime() == x.getTime()) {
                    workflows_click_array[progress_counter] = {
                        late: ""
                        //, late_id: ""
                        , progress: ""
                        //, progress_id: ""
                        , finished: ""
                        //, finished_id: ""
                    };
                    workflows_click_array[0].late = parent.msg("list.empty")
                }
            };
            //prepare in progress workflow list to popup
            for (var i = 0; i < parent.options.encoursList_per_Day.length; i++) {
                if (parent.options.encoursList_per_Day[i].array.length > 0) {
                    for (var j = 0; j < parent.options.encoursList_per_Day[i].array.length; j++) {
                        if (parent.options.encoursList_per_Day[i].date.getTime() == x.getTime()) {
                            //  var lien2 = 'window.open("' + parent.options.domaina + '/share/page/workflow-details?workflowId=' + parent.options.encoursList_per_Day[i].array[j].id + '")';
                            //   "<li onclick=" + lien2 + ">" + parent.options.encoursList_per_Day[i].array[j].message + "</li>";
                            if (!workflows_click_array[progress_counter]) {
                                workflows_click_array[progress_counter] = {
                                    late: ""
                                   // , late_id: ""
                                    , progress: ""
                                   // , progress_id: ""
                                    , finished: ""
                                   // , finished_id: ""
                                };
                                workflows_click_array[progress_counter].progress = parent.options.encoursList_per_Day[i].array[j].message + "/" + parent.options.encoursList_per_Day[i].array[j].id;
                                //workflows_click_array[progress_counter].progress_id = parent.options.encoursList_per_Day[i].array[j].id;
                                progress_counter++;
                            }
                            else {
                                workflows_click_array[progress_counter].progress = parent.options.encoursList_per_Day[i].array[j].message + "/" + parent.options.encoursList_per_Day[i].array[j].id;
                                //workflows_click_array[progress_counter].progress_id = parent.options.encoursList_per_Day[i].array[j].id;
                                progress_counter++;
                            }
                        }
                    }
                }
                if (parent.options.encoursList_per_Day[i].array.length <= 0 && parent.options.encoursList_per_Day[i].date.getTime() == x.getTime()) workflows_click_array[0].progress = parent.msg("list.empty")
            };
            //prepare finished workflow list to popup
            for (var i = 0; i < parent.options.termineList_per_Day.length; i++) {
                if (parent.options.termineList_per_Day[i].array.length > 0) {
                    for (var j = 0; j < parent.options.termineList_per_Day[i].array.length; j++) {
                        if (parent.options.termineList_per_Day[i].date.getTime() == x.getTime()) {
                            if (!workflows_click_array[finished_counter]) {
                                workflows_click_array[finished_counter] = {
                                    late: ""
                                   // , late_id: ""
                                    , progress: ""
                                    //, progress_id: ""
                                    , finished: ""
                                    //, finished_id: ""
                                };
                                workflows_click_array[finished_counter].finished = parent.options.termineList_per_Day[i].array[j].message + "/" + parent.options.termineList_per_Day[i].array[j].id;
                               // workflows_click_array[finished_counter].finished_id = parent.options.termineList_per_Day[i].array[j].id;
                                finished_counter++;
                            }
                            else {
                                workflows_click_array[finished_counter].finished = parent.options.termineList_per_Day[i].array[j].message + "/" +  parent.options.termineList_per_Day[i].array[j].id;
                                //workflows_click_array[finished_counter].finished_id = parent.options.termineList_per_Day[i].array[j].id;
                                finished_counter++;
                            }
                        }
                    }
                }
                if (parent.options.termineList_per_Day[i].array.length <= 0 && parent.options.termineList_per_Day[i].date.getTime() == x.getTime()) workflows_click_array[0].finished = parent.msg("list.empty")
            };
            //set dataTable and paginator for popup
            YAHOO.example.MultipleFeatures = function () {
                lateFormatter = function(elLiner, oRecord, oColumn, oData) {
                        elLiner.innerHTML = oRecord.getData("late").split("/")[0] + ' <input type="hidden" value=' + oRecord.getData("late").split("/")[1] +'>'; 
                };

                // Add the custom formatter to the shortcuts
                YAHOO.widget.DataTable.Formatter.late = lateFormatter;
                progressFormatter = function(elLiner, oRecord, oColumn, oData) {
                        elLiner.innerHTML = oRecord.getData("progress").split("/")[0] + ' <input type="hidden" value=' + oRecord.getData("progress").split("/")[1] +'>'; 
                };

                // Add the custom formatter to the shortcuts
                YAHOO.widget.DataTable.Formatter.late = lateFormatter;
                finishedFormatter = function(elLiner, oRecord, oColumn, oData) {
                        elLiner.innerHTML = oRecord.getData("finished").split("/")[0] + ' <input type="hidden" value=' + oRecord.getData("finished").split("/")[1] +'>'; 
                };

                // Add the custom formatter to the shortcuts
                YAHOO.widget.DataTable.Formatter.lateFormatter = lateFormatter;
                YAHOO.widget.DataTable.Formatter.progressFormatter = progressFormatter;
                YAHOO.widget.DataTable.Formatter.finishedFormatter = finishedFormatter;
                var myColumnDefs = [
                    {
                        key: "late"
                        , label: "<div style='font-weight: bold;'>"  + parent.msg("list.retard") + "</div>"
                        , width: 190
                        , resizeable: false
                        , sortable: false
                        ,formatter:"lateFormatter"
                    }
                    , {
                        key: "progress"
                        , label: "<div style='font-weight: bold;'>"  + parent.msg("list.inprogress") + "</div>" 
                        , width: 190
                        , resizeable: false
                        , sortable: false
                        ,formatter:"progressFormatter"
                    }
                    , {
                        key: "finished" + '<input type="hidden" value=' + "finished_id" + "/>"
                        , label: "<div style='font-weight: bold;'>"  + parent.msg("list.finished") + "</div>" 
                        , width: 190
                        , resizeable: false
                        , sortable: false
                        ,formatter:"finishedFormatter"
                    }
                    
                ];
                //console.log(workflows_click_array);
                var myDataSource = new YAHOO.util.DataSource(workflows_click_array);
                var myConfigs = {
                    paginator: new YAHOO.widget.Paginator({
                        rowsPerPage: 5
                        , pageLinks: 5
                        , containers: [parent.id + "-paginator"]
                        , template: parent.msg("pagination.template")
                        , pageReportTemplate: parent.msg("pagination.template.page-report")
                        , previousPageLinkLabel: parent.msg("pagination.previousPageLinkLabel")
                        , nextPageLinkLabel: parent.msg("pagination.nextPageLinkLabel")
                    })
                , }
                var myDataTable = new YAHOO.widget.DataTable(parent.id + "-bdPOPUP", myColumnDefs, myDataSource, myConfigs);
                myDataTable.subscribe("cellClickEvent", myDataTable.onEventSelectCell);
                myDataTable.subscribe("cellClickEvent", function (oArgs) {
                    var cell_message = oArgs.target.children[0].children[0].value;
                    //console.log( oArgs.target.children[0].children[0].value);
                    if (cell_message && cell_message != 'undefined') {	
							var domain = Alfresco.constants.PROXY_URI.split("/");
							domain = domain[0] + "//" + domain[2];
                            window.open(domain + "/share/page/workflow-details?workflowId=" + cell_message);
                        
                    }
                });
               myDataTable.subscribe("cellMouseoverEvent", myDataTable.onEventHighlightCell);
               myDataTable.subscribe("cellMouseoutEvent", myDataTable.onEventUnhighlightCell);
                return {
                    oDS: myDataSource
                    , oDT: myDataTable
                };
            }();
            //show popup
            myDialog.render();
            myDialog.show();
        }
        , /**
         * autocomplete workflow name in the input.
         * @method _autocomplete
         * 
         */
        autocomplete: function _autocomplete() {
            //transform workflow id/name (exp: activiti$activitiAdhoc:1:4/Nouvelle tâche,activiti$activitiInvitationModerated:1:23/Invitation - Modérée,...)
            //from string to list
            var workflowDefinitionsNames  = parent.options.workflow_definition_list.split(",");
			var workflowDefinitionsIds = [];
			var devider = [];
            //separate names and ids (names in res and ids in workflowDefinitionsIds)
            for (var i = 0; i < workflowDefinitionsNames.length; i++) {
                devider[i] = workflowDefinitionsNames[i].split("/")[1];
                workflowDefinitionsIds[i] = workflowDefinitionsNames[i].split("/")[0];
            };
            parent.options.arrayWorkflowDefinitions = [];
            parent.options.arrayWorkflowDefinitionsIds = [];
            //adjust autocomplete list width to the longest workflow definition name
            var myAutocomplete_size = devider[0].length;
            for (var i = 0; i < devider.length; i++) {
                if (devider[i].length > myAutocomplete_size) {
                    myAutocomplete_size = devider[i].length;
                }
                parent.options.arrayWorkflowDefinitions.push(devider[i]);
                parent.options.arrayWorkflowDefinitionsIds[devider[i]] = workflowDefinitionsIds[i];
            };
            Dom.get(parent.id + "-myAutoComplete").style.width = myAutocomplete_size + "em";
            /**
             * Use a LocalDataSource
             */
            var oDS = new YAHOO.util.LocalDataSource(parent.options.arrayWorkflowDefinitions);
            /**
             * Instantiate the AutoComplete
             */
            var oAC = new YAHOO.widget.AutoComplete(this.id + "-myInput", this.id + "-myContainer", oDS);
            oAC.minQueryLength = 0;
            oAC.maxResultsDisplayed = 12;
            oAC.prehighlightClassName = "yui-ac-prehighlight";
            oAC.useShadow = true;
            oAC.itemSelectEvent.subscribe(this.autocomplete_chart)
            return {
                oDS: oDS
                , oAC: oAC
            };
        }
        , /**
         * Event when workflow name is selected.
         * @method _autocomplete_chart
         * 
         */
        autocomplete_chart: function _autocomplete_chart() {
            Dom.removeClass(parent.id + "-start-date", "hidden");
            Dom.removeClass(parent.id + "-end-date", "hidden");
            Dom.removeClass(parent.id + "-hr", "hidden");
            Dom.get(parent.id + '-global-container').style.height = '116px';
            
            //check if start date and end date are selected
            if (Dom.get(parent.id + "2-startd").value && Dom.get(parent.id + "-startd").value) {
                //check if the 2 dates are valid and the end date is after the start date
                if (Dom.get(parent.id + "2-startd").value >= Dom.get(parent.id + "-startd").value && parent.options.startDateValidity && parent.options.endDateValidity) {
                	   var ticket;
                       //console.log("eee");
                       urlTicket =  Alfresco.constants.PROXY_URI + 'session/ticket.json';
                       $.ajax({
                           type: "Get",
                           url: urlTicket,
                           success: function (ticket) {
                             ticket = ticket.responseText;
                             $.ajax({
                                 type: "Get",
                                 url:  /*  'http://127.0.0.1:8080'  */  Alfresco.constants.PROXY_URI + 'api/workflow-definitions/' + parent.options.arrayWorkflowDefinitionsIds[Dom.get(parent.id + "-myInput").value] + '/workflow-instances?alf_ticket='+ ticket,
                                 success: function (data) {
                                 	var inProgressList = data;
                                 	var complitedList;
                                     //get all workflow completed instances of selected workflow definition
                                     $.ajax({
                                         type: "Get",
                                         url:   Alfresco.constants.PROXY_URI + 'api/workflow-definitions/' + parent.options.arrayWorkflowDefinitionsIds[Dom.get(parent.id + "-myInput").value] + '/workflow-instances?state=COMPLETED&alf_ticket='+ ticket,
                                         success: function (data) {
                                         	 complitedList = data;
                                             parent.options.list_of_all_workflow = inProgressList.data.concat(complitedList.data);
                                             parent.makeChart();
                                           
                                         }, 
                                         async: true 
                                     })
                                   }, 
                                 async: true 
                             })
                           }, 
                           error : function (ticket) {
                           	ticket = ticket.responseText;
                               $.ajax({
                                   type: "Get",
                                   url:  /*  'http://127.0.0.1:8080'  */  Alfresco.constants.PROXY_URI + 'api/workflow-definitions/' + parent.options.arrayWorkflowDefinitionsIds[Dom.get(parent.id + "-myInput").value] + '/workflow-instances?alf_ticket='+ ticket,
                                   success: function (data) {
                                   	var inProgressList = data;
                                   	var complitedList;
                                       //get all workflow completed instances of selected workflow definition
                                       $.ajax({
                                           type: "Get",
                                           url:   Alfresco.constants.PROXY_URI + 'api/workflow-definitions/' + parent.options.arrayWorkflowDefinitionsIds[Dom.get(parent.id + "-myInput").value] + '/workflow-instances?state=COMPLETED&alf_ticket='+ ticket,
                                           success: function (data) {
                                           	   complitedList = data;
                                               parent.options.list_of_all_workflow = inProgressList.data.concat(complitedList.data);
                                               parent.makeChart();
                                             
                                           }, 
                                           async: true 
                                       })
                                     }, 
                                   async: true 
                               })
                         }, 
                         async: true // <- this turns it into synchronous
                         })                 
                    //remove failure messages and colors
                    Dom.removeClass(parent.id + "-start-cntrl-date", "invalid");
                    Dom.addClass(parent.id + "-start-cntrl-alf-id2", "yui-overlay");
                    Dom.removeClass(parent.id + "2-start-cntrl-date", "invalid");
                    Dom.addClass(parent.id + "2-start-cntrl-alf-id2", "yui-overlay");
                }
                //if 2 dates are valid but start date is after and date show error popup
                else if (Dom.get(parent.id + "2-startd").value < Dom.get(parent.id + "-startd").value && parent.options.startDateValidity && parent.options.endDateValidity) {
                    Dom.removeClass(parent.id + "-start-cntrl-date", "invalid");
                    Dom.addClass(parent.id + "-start-cntrl-alf-id2", "yui-overlay");
                    Dom.removeClass(parent.id + "2-start-cntrl-date", "invalid");
                    Dom.addClass(parent.id + "2-start-cntrl-alf-id2", "yui-overlay");
                    Dom.get(parent.id + "-chartContainer").innerHTML = '<div id="prompt" style="border-top: 0px; border-bottom: 0px;"><div id="' + parent.id + '-dialog2" class="yui-pe-content"><div id="' + parent.id 
                                                                        + '-titlePOPUP2" class="hd">' + parent.msg("message.failure") + '</div><div id="' 
                                                                        + parent.id + '-bdPOPUP2" class="bd"><div  style="line-height: 1.231;margin: auto;width: 32%;">' 
                                                                        + parent.msg("error.periode") + '</div></div></div></div>';
                    var handleCancel = function () {
                        myDialogError.hide();
                    };
                    var myDialogError = new YAHOO.widget.Dialog(parent.id + "-dialog2", {
                        width: "30em"
                        ,modal:true
                        , fixedcenter: true
                        , visible: false
                        , constraintoviewport: true
                        , buttons: [{
                            text: "OK"
                            , handler: handleCancel
                            , isDefault: true
                        }]
                    });
                    myDialogError.render();
                    myDialogError.show();
                }
                //if selected dates are invalid show failure msg
                else {
                    if (!parent.options.startDateValidity) {
                        Dom.addClass(parent.id + "-start-cntrl-date", "invalid");
                        Dom.removeClass(parent.id + "-start-cntrl-alf-id2", "yui-overlay");
                    }
                    if (!parent.options.endDateValidity) {
                        Dom.addClass(parent.id + "2-start-cntrl-date", "invalid");
                        Dom.removeClass(parent.id + "2-start-cntrl-alf-id2", "yui-overlay");
                    }
                }
            }
        }
    });
})();