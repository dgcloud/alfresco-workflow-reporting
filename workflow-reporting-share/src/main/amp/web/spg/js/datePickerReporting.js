/**
 * This is a custom date-picker.js, we have just add spesific code in _handlePickerChange and _handleFieldChange methods
 *the goal of these additions is to drow the chart
 *
 * SPG_DatePicker component.
 * 
 * @namespace Alfresco
 * @class DatePicker2
 */
if (typeof DatePicker2 == "undefined" || !DatePicker2) {
    var DatePicker2 = {};
}
(function () {
    /**
     * YUI Library aliases
     */
    var Dom = YAHOO.util.Dom
        , Event = YAHOO.util.Event
        , KeyListener = YAHOO.util.KeyListener;
    /**
     * Alfresco Slingshot aliases
     */
    var $html = Alfresco.util.encodeHTML;
    /**
     * DatePicker constructor.
     * 
     * @param {String}
     *            htmlId The HTML id of the parent element
     * @param {String}
     *            currentValueHtmlId The HTML id of the parent element
     * @return {DatePicker2} The new DatePicker instance
     * @constructor
     */
    DatePicker2 = function (htmlId, currentValueHtmlId) {
        // Mandatory properties
        this.name = "DatePicker2";
        this.id = htmlId;
        this.currentValueHtmlId = currentValueHtmlId;
        /* Register this component */
        Alfresco.util.ComponentManager.register(this);
        /* Load YUI Components */
        Alfresco.util.YUILoaderHelper.require(["button", "calendar"], this.onComponentsLoaded, this);
        // Initialise prototype properties
        this.widgets = {};
        return this;
    };
    DatePicker2.prototype = {
        /**
         * Object container for initialization options
         * 
         * @property options
         * @type object
         */
        options: {
            /**
             * The current value
             * 
             * @property currentValue
             * @type string
             */
            currentValue: ""
            , /**
             * Flag to determine whether a time field should be visible
             * 
             * @property showTime
             * @type boolean
             * @default false
             */
            showTime: false
            , /**
             * Flag to determine whether the picker is in disabled mode
             * 
             * @property disabled
             * @type boolean
             * @default false
             */
            disabled: false
            , /**
             * Flag to indicate whether the field is mandatory
             * 
             * @property mandatory
             * @type boolean
             * @default false
             */
            mandatory: false
        }
        , /**
         * Object container for storing YUI widget instances.
         * 
         * @property widgets
         * @type object
         */
        widgets: null
        , /**
         * Set multiple initialization options at once.
         * 
         * @method setOptions
         * @param obj
         *            {object} Object literal specifying a set of options
         * @return {DatePicker2} returns 'this' for method chaining
         */
        setOptions: function DatePicker_setOptions(obj) {
            this.options = YAHOO.lang.merge(this.options, obj);
            return this;
        }
        , /**
         * Set messages for this component.
         * 
         * @method setMessages
         * @param obj
         *            {object} Object literal specifying a set of messages
         * @return {DatePicker2} returns 'this' for method chaining
         */
        setMessages: function DatePicker_setMessages(obj) {
            Alfresco.util.addMessages(obj, this.name);
            return this;
        }
        , /**
         * Fired by YUILoaderHelper when required component script files have
         * been loaded into the browser.
         * 
         * @method onComponentsLoaded
         */
        onComponentsLoaded: function DatePicker_onComponentsLoaded() {
            Event.onContentReady(this.id, this.onReady, this, true);
        }
        , /**
         * Fired by YUI when parent element is available for scripting.
         * Component initialisation, including instantiation of YUI widgets and
         * event listener binding.
         * 
         * @method onReady
         */
        onReady: function DatePicker_onReady() {
            var theDate = null;
            if (this.options.currentValue == null || this.options.currentValue === "") {
                // MNT-2214 fix, check for prevously entered value
                this.options.currentValue = Dom.get(this.currentValueHtmlId).value;
            }
            // calculate current date
            if (this.options.currentValue !== null && this.options.currentValue !== "") {
                // MNT-9693 - use the showTime option to trigger the ignoreTime
                // flag (note the boolean reversing)
                theDate = Alfresco.util.fromISO8601(this.options.currentValue, !this.options.showTime);
            }
            else {
                theDate = new Date();
            }
            var page = (theDate.getMonth() + 1) + "/" + theDate.getFullYear();
            var selected = (theDate.getMonth() + 1) + "/" + theDate.getDate() + "/" + theDate.getFullYear();
            var dateEntry = theDate.toString(this._msg("form.control.date-picker.entry.date.format"));
            var timeEntry = theDate.toString(this._msg("form.control.date-picker.entry.time.format"));
            // populate the input fields
            if (this.options.currentValue !== "") {
                // show the formatted date
                Dom.get(this.id + "-date").value = dateEntry;
                if (this.options.showTime) {
                    Dom.get(this.id + "-time").value = timeEntry;
                }
            }
            // construct the picker
            function getCalendarControlConfiguration() {
                var a = {
                    strings: {
                        month: Alfresco.util.message("calendar.widget_control_config.label_month"),
                        year: Alfresco.util.message("calendar.widget_control_config.label_year"),
                        submit: Alfresco.util.message("button.ok"),
                        cancel: Alfresco.util.message("button.cancel"),
                        invalidYear: Alfresco.util.message("calendar.widget_control_config.label_invalid_year")
                    },
                    monthFormat: YAHOO.widget.Calendar.SHORT,
                    initialFocus: "year"
                };
                return a
            };
            var navConfig = getCalendarControlConfiguration();
            this.widgets.calendar = new YAHOO.widget.Calendar(this.id, this.id, {
                title: this._msg("form.control.date-picker.choose")
                , close: true
                , navigator: navConfig
            });
            this.widgets.calendar.cfg.setProperty("pagedate", page);
            this.widgets.calendar.cfg.setProperty("selected", selected);
            Alfresco.util.calI18nParams(this.widgets.calendar);
            // setup events
            this.widgets.calendar.selectEvent.subscribe(this._handlePickerChange, this, true);
            this.widgets.calendar.hideEvent.subscribe(function () {
                // Focus icon after calendar is closed
                Dom.get(this.id + "-icon").focus();
            }, this, true);
            Event.addListener(this.id + "-date", "keyup", this._handleFieldChange, this, true);
            Event.addListener(this.id + "-time", "keyup", this._handleFieldChange, this, true);
            var iconEl = Dom.get(this.id + "-icon");
            if (iconEl) {
                // setup keyboard enter events on the image instead of the link
                // to get focus outline displayed
                Alfresco.util.useAsButton(iconEl, this._showPicker, null, this);
                Event.addListener(this.id + "-icon", "click", this._showPicker, this, true);
            }
            // register a validation handler for the date entry field so that
            // the submit
            // button disables when an invalid date is entered
            YAHOO.Bubbling.fire("registerValidationHandler", {
                fieldId: this.id + "-date"
                , handler: Alfresco.forms.validation.validDateTime
                , when: "keyup"
            });
            // register a validation handler for the time entry field (if
            // applicable)
            // so that the submit button disables when an invalid date is
            // entered
            if (this.options.showTime) {
                YAHOO.Bubbling.fire("registerValidationHandler", {
                    fieldId: this.id + "-time"
                    , handler: Alfresco.forms.validation.validDateTime
                    , when: "keyup"
                });
            }
            var theMaxDate = new Date();
            var maxDate = (theMaxDate.getMonth() + 1) + "/" + theMaxDate.getDate() + "/" + theMaxDate.getFullYear();
             this.widgets.calendar.cfg.setProperty("maxdate",maxDate,false);
            // render the calendar control
            this.widgets.calendar.render();
            // If value was set in visible fields, make sure they are validated
            // and put in the hidden field as well
            if (this.options.currentValue !== "") {
                this._handleFieldChange(null);
            }
        }
        , /**
         * Handles the date picker icon being clicked.
         * 
         * @method _showPicker
         * @param event
         *            The event that occurred
         * @private
         */
        _showPicker: function DatePicker__showPicker(event) {
            // show the popup calendar widget
            this.widgets.calendar.show();
        }
        , /**
         * Handles the date being changed in the date picker YUI control.
         * 
         * @method _handlePickerChange
         * @param type
         * @param args
         * @param obj
         * @private
         */
        _handlePickerChange: function DatePicker__handlePickerChange(type, args, obj) {
            // alert(Dom.get(parent.id+"-myInput"));
            // update the date field
            var selected = args[0];
            var selDate = this.widgets.calendar.toDate(selected[0]);
            var dateEntry = selDate.toString(this._msg("form.control.date-picker.entry.date.format"));
            Dom.get(this.id + "-date").value = dateEntry;
            // update the time field if necessary
            if (this.options.showTime) {
                var time = Dom.get(this.id + "-time").value;
                if (time.length > 0) {
                    var dateTime = Dom.get(this.id + "-date").value + " " + time;
                    var dateTimePattern = this._msg("form.control.date-picker.entry.date.format") + " " + this._msg("form.control.date-picker.entry.time.format");
                    selDate = Date.parseExact(dateTime, dateTimePattern);
                }
            }
            // console.log(selected.toString()+"//////"+selDate+"////"+dateEntry
            // + "//");
            // if we have a valid date, convert to ISO format and set value on
            // hidden field
            if (selDate != null) {
                Dom.removeClass(this.id + "-date", "invalid");
                Dom.addClass(this.id + "-alf-id2", "yui-overlay");
                if (this.options.showTime) {
                    Dom.removeClass(this.id + "-time", "invalid");
                }
                var isoValue = "";
                // Check if time should be submitted as well
                if (this.options.submitTime) {
                    isoValue = Alfresco.util.toISO8601(selDate, {
                        "milliseconds": true
                    });
                }
                else {
                    isoValue = Alfresco.util.toISO8601(selDate, {
                        "selector": "date"
                    });
                }
                Dom.get(this.currentValueHtmlId).value = isoValue;
                if (Alfresco.logger.isDebugEnabled()) Alfresco.logger.debug("Hidden field '" + this.currentValueHtmlId + "' updated to '" + isoValue + "'");
                // always inform the forms runtime that the control value has
                // been updated
                YAHOO.Bubbling.fire("mandatoryControlValueUpdated", this);
            }
            else {
                Dom.addClass(this.id + "-date", "invalid");
                if (this.options.showTime) {
                    Dom.addClass(this.id + "-time", "invalid");
                }
            }
            // Hide calendar if the calendar was open (Unfortunately there is no
            // proper yui api method for this)
            if (Dom.getStyle(this.id, "display") != "none") {
                this.widgets.calendar.hide();
            }
            
          //check that start date is not manuelly changed 
            if ('"' + parent.id + "-start-cntrl-date" + '"' === '"' + this.id + "-date" + '"') {
                parent.options.startDateValidity = true
            }
            //check that end date is not manuelly changed                 
            if ('"' + parent.id + "2-start-cntrl-date" + '"' === '"' + this.id + "-date" + '"') {
                parent.options.endDateValidity = true
            }
            //we react only when three inputs are filled 
            if (Dom.get(parent.id + "-myInput").value && Dom.get(parent.id + "-startd").value && Dom.get(parent.id + "2-startd").value) {
                //when the selected workflow definition name is valid and the perid is valid show chart 
                if (parent.options.endDateValidity && parent.options.startDateValidity && Dom.get(parent.id + "2-startd").value >= Dom.get(parent.id + "-startd").value && contains(parent.options.arrayWorkflowDefinitions, Dom.get(parent.id + "-myInput").value)) {
                    var ticket;
                    //console.log("eee");
                    urlTicket =  Alfresco.constants.PROXY_URI + 'session/ticket.json';
                    $.ajax({
                        type: "Get",
                        url: urlTicket,
                        success: function (ticket) {
                          ticket = ticket.responseText;
                          $.ajax({
                              type: "Get",
                              url:  /*  'http://127.0.0.1:8080'  */  Alfresco.constants.PROXY_URI + 'api/workflow-definitions/' + parent.options.arrayWorkflowDefinitionsIds[Dom.get(parent.id + "-myInput").value] + '/workflow-instances?alf_ticket='+ ticket,
                              success: function (data) {
                              	var inProgressList = data;
                              	var complitedList;
                                  //get all workflow completed instances of selected workflow definition
                                  $.ajax({
                                      type: "Get",
                                      url:   Alfresco.constants.PROXY_URI + 'api/workflow-definitions/' + parent.options.arrayWorkflowDefinitionsIds[Dom.get(parent.id + "-myInput").value] + '/workflow-instances?state=COMPLETED&alf_ticket='+ ticket,
                                      success: function (data) {
                                      	  complitedList = data;
                                          parent.options.list_of_all_workflow = inProgressList.data.concat(complitedList.data);
                                          parent.makeChart();
                                        
                                      }, 
                                      async: true 
                                  })
                                }, 
                              async: true 
                          })
                        }, 
                        error : function (ticket) {
                        	ticket = ticket.responseText;
                            $.ajax({
                                type: "Get",
                                url:  /*  'http://127.0.0.1:8080'  */  Alfresco.constants.PROXY_URI + 'api/workflow-definitions/' + parent.options.arrayWorkflowDefinitionsIds[Dom.get(parent.id + "-myInput").value] + '/workflow-instances?alf_ticket='+ ticket,
                                success: function (data) {
                                	var inProgressList = data;
                                	var complitedList;
                                    //get all workflow completed instances of selected workflow definition
                                    $.ajax({
                                        type: "Get",
                                        url:   Alfresco.constants.PROXY_URI + 'api/workflow-definitions/' + parent.options.arrayWorkflowDefinitionsIds[Dom.get(parent.id + "-myInput").value] + '/workflow-instances?state=COMPLETED&alf_ticket='+ ticket,
                                        success: function (data) {
                                        	  complitedList = data;
                                            parent.options.list_of_all_workflow = inProgressList.data.concat(complitedList.data);
                                            parent.makeChart();
                                          
                                        }, 
                                        async: true 
                                    })
                                  }, 
                                async: true 
                            })
                      }, 
                      async: true // <- this turns it into synchronous
                      })                 
                    //remove failure messages and colors
                    Dom.removeClass(parent.id + "-start-cntrl-date", "invalid");
                    Dom.addClass(parent.id + "-start-cntrl-alf-id2", "yui-overlay");
                    Dom.removeClass(parent.id + "2-start-cntrl-date", "invalid");
                    Dom.addClass(parent.id + "2-start-cntrl-alf-id2", "yui-overlay");
                }
                //if 2 dates are valid but start date is after and date show error popup
                else if (parent.options.endDateValidity && parent.options.startDateValidity && Dom.get(parent.id + "2-startd").value < Dom.get(parent.id + "-startd").value && contains(parent.options.arrayWorkflowDefinitions, Dom.get(parent.id + "-myInput").value)) {
                    Dom.get(parent.id + "-chartContainer").innerHTML = '<div id="prompt" style="border-top: 0px; border-bottom: 0px;"><div id="' + parent.id + '-dialog2" class="yui-pe-content"><div id="' + parent.id 
                    													+ '-titlePOPUP2" class="hd">' + parent.msg("message.failure") + '</div><div id="' 
                    													+ parent.id + '-bdPOPUP2" class="bd"><div  style="line-height: 1.231;margin: auto;width: 32%;">' 
                    													+ parent.msg("error.periode") + '</div></div></div></div>';
                    var handleCancel = function () {
                        myDialogError.hide();
                    };
                    var myDialogError = new YAHOO.widget.Dialog(parent.id + "-dialog2", {
                        width: "30em"
                        ,modal:true
                        , fixedcenter: true
                        , visible: false
                        , constraintoviewport: true
                        , buttons: [{
                            text: "OK"
                            , handler: handleCancel
                            , isDefault: true
						}]
                    });
                    myDialogError.render();
                    myDialogError.show();
                }
                //check which input contains an error
                else {
                	if (!contains(parent.options.arrayWorkflowDefinitions, Dom.get(parent.id + "-myInput").value)) {
                        Dom.setXY(parent.id + "-alf-id2", [Dom.getX(parent.id + "-myInput"), Dom.getY(parent.id + "-myInput") - 44]);
                        Dom.removeClass(parent.id + "-alf-id2", "yui-overlay");
                        if (parent.options.startDateValidity) {

                            Dom.addClass(parent.id + "-start-cntrl-alf-id2", "yui-overlay");
                            Dom.removeClass(parent.id + "-start-cntrl-date", "invalid");
                        }
                        if (parent.options.endDateValidity) {
                            Dom.removeClass(parent.id + "2-start-cntrl-date", "invalid");
                            Dom.addClass(parent.id + "2-start-cntrl-alf-id2", "yui-overlay");
                        }
                    }
                    
                    if (!parent.options.startDateValidity) {
                        
                        Dom.removeClass(parent.id + "-start-cntrl-alf-id2", "yui-overlay");
                        Dom.addClass(parent.id + "-start-cntrl-date", "invalid");
                    }
                    if (!parent.options.endDateValidity) {
                        Dom.addClass(parent.id + "2-start-cntrl-date", "invalid");
                        Dom.removeClass(parent.id + "2-start-cntrl-alf-id2", "yui-overlay");
                    }
                }
            }
        }
        , /**
         * Handles the date or time being changed in either input field.
         * 
         * @method _handleFieldChange
         * @param event
         *            The event that occurred
         * @private
         */
        _handleFieldChange: function DatePicker__handleFieldChange(event) {
            var changedDate = Dom.get(this.id + "-date").value;
            if (changedDate.length > 0) {
                // Only set for actual value changes so tab or shift events
                // doesn't remove the "text selection" of the input field
                if (event == undefined || (event.keyCode != KeyListener.KEY.TAB && event.keyCode != KeyListener.KEY.SHIFT)) {
                    // convert to format expected by YUI
                    var parsedDate = Date.parseExact(changedDate, this._msg("form.control.date-picker.entry.date.format"));
                    if (parsedDate != null) {
                    	var theMaxDate = new Date();
                        theMaxDate.setHours(0, 0, 0, 0);
                        function toDate(dateStr) {
                            var parts = dateStr.split("/");
                            return new Date(parts[2], parts[1] - 1, parts[0]);
                        }
                        validationChangedDate = toDate(changedDate);
                        if(validationChangedDate >= theMaxDate){
                        Dom.get(parent.id + "-chartContainer").innerHTML = '<div id="' + parent.id + '-dialog2" class="yui-pe-content"><div id="' + parent.id + '-titlePOPUP2" class="hd">' + parent.msg("message.failure") + '</div><div id="' + parent.id + '-bdPOPUP2" class="bd"><div  style="margin: auto;width: 32%;">' + parent.msg("error.periode") + '</div></div></div>';
                        var handleCancel = function () {
                            myDialogError.hide();
                        };
                        var myDialogError = new YAHOO.widget.Dialog(parent.id + "-dialog2", {
                            width: "30em"
                            ,modal:true	
                            , fixedcenter: true
                            , visible: false
                            , constraintoviewport: true
                            , buttons: [{
                                text: "OK"
                                , handler: handleCancel
                                , isDefault: true
            				}]
                        });
                        myDialogError.render();
                        myDialogError.show();
                        }
                        this.widgets.calendar.select((parsedDate.getMonth() + 1) + "/" + parsedDate.getDate() + "/" + parsedDate.getFullYear());
                        var selectedDates = this.widgets.calendar.getSelectedDates();
                        if (selectedDates.length > 0) {
                        	//if filled date is valid remove failure behavior
						    Dom.removeClass(this.id + "-date", "invalid");
                            Dom.addClass(this.id + "-alf-id2", "yui-overlay");
                            //if start date is manually filled and entered value is correct, startDateValidity become true
                            if ('"' + parent.id + "-start-cntrl-date" + '"' === '"' + this.id + "-date" + '"') {
                                parent.options.startDateValidity = true
                            }
                            //if end date is manually filled and entered value is correct, endDateValidity become true
                            if ('"' + parent.id + "2-start-cntrl-date" + '"' === '"' + this.id + "-date" + '"') {
                                parent.options.endDateValidity = true
                            }
                            var firstDate = selectedDates[0];
                            this.widgets.calendar.cfg.setProperty("pagedate", (firstDate.getMonth() + 1) + "/" + firstDate.getFullYear());
                            this.widgets.calendar.render();
                            // NOTE: we don't need to check the time value in
                            // here as the _handlePickerChange
                            // function gets called as well as a result of
                            // rendering the picker above,
                            // that's also why we don't update the hidden field
                            // in here either.
                        }
                    }
                    else {
                    	//if filled date is invalid, we add failure behavior
                        Dom.addClass(this.id + "-date", "invalid");
                        Dom.removeClass(this.id + "-alf-id2", "yui-overlay");

                        //if start date is manually filled and entered value is incorrect, startDateValidity become false
                        if ('"' + parent.id + "-start-cntrl-date" + '"' === '"' + this.id + "-date" + '"') {
                            parent.options.startDateValidity = false
                        }

                        //if end date is manually filled and entered value is incorrect, endDateValidity become false
                        if ('"' + parent.id + "2-start-cntrl-date" + '"' === '"' + this.id + "-date" + '"') {
                            parent.options.endDateValidity = false
                        }

                        var inputId = this.id;
                        //adjust failure msg position
                        Dom.setXY(inputId + "-alf-id2", [Dom.getX(inputId + "-date"), Dom.getY(inputId + "-date") - 44]);
                        

                        //onfocus of input of workflow name 
                    	Dom.get(inputId + "-date").onfocus = function () {
                        	if (contains(Dom.get(inputId + "-date").classList, "invalid")) Dom.removeClass(inputId + "-alf-id2", "yui-overlay");
                        	//adjust failure msg position
                        	Dom.setXY(inputId + "-alf-id2", [Dom.getX(inputId + "-date"), Dom.getY(inputId + "-date") - 44]);
                    	};
                    	//onblur of input of workflow name 
                    	Dom.get(inputId + "-date").onblur = function () {
                        	if (contains(Dom.get(inputId + "-date").classList, "invalid")) Dom.addClass(inputId + "-alf-id2", "yui-overlay");
                        	//adjust failure msg position
                        	Dom.setXY(inputId + "-alf-id2", [Dom.getX(inputId + "-date"), Dom.getY(inputId + "-date") - 44]);
                    	};

                    }
                }
            }
            else {
                // when the date is completely cleared remove the hidden field
                // and remove the invalid class
                Dom.removeClass(this.id + "-date", "invalid");
                Dom.addClass(this.id + "-alf-id2", "yui-overlay");
                if ('"' + parent.id + "-start-cntrl-date" + '"' === '"' + this.id + "-date" + '"') {
                    parent.options.startDateValidity = true
                }
                if ('"' + parent.id + "2-start-cntrl-date" + '"' === '"' + this.id + "-date" + '"') {
                    parent.options.endDateValidity = true
                }
                Dom.get(this.currentValueHtmlId).value = "";
                if (Alfresco.logger.isDebugEnabled()) Alfresco.logger.debug("Hidden field '" + this.currentValueHtmlId + "' has been reset");
                // inform the forms runtime that the control value has been
                // updated
                if (this.options.mandatory || YAHOO.env.ua.ie) {
                    YAHOO.Bubbling.fire("mandatoryControlValueUpdated", this);
                }
            }
        }
        , /**
         * Gets a custom message
         * 
         * @method _msg
         * @param messageId
         *            {string} The messageId to retrieve
         * @return {string} The custom message
         * @private
         */
        _msg: function DatePicker__msg(messageId) {
            return Alfresco.util.message.call(this, messageId, "DatePicker2", Array.prototype.slice.call(arguments).slice(1));
        }
    };
})();