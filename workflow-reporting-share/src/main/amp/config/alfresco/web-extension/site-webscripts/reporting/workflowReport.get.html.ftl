<@markup id="js">
<@script type="text/javascript" src="${page.url.context}/res/spg/js/jquery.js"/>
<@script type="text/javascript" src="${page.url.context}/res/spg/js/jquery-ui.js"/>
<@script type="text/javascript" src="${page.url.context}/res/spg/js/Chart.js"/>
<@script type="text/javascript" src="${url.context}/res/spg/js/datePickerReporting.js"/>
<@script type="text/javascript" src="${page.url.context}/res/spg/js/workflowReporting.js"/>
<@script   type="text/javascript"  src="${url.context}/res/components/form/date.js" group="form"/>  
<@script type="text/javascript" src="${url.context}/res/components/form/period.js" group="form"/>
<@script src="${url.context}/res/yui/calendar/calendar.js" group="form"/> 
</@>
<#-- Stylesheet Dependencies-->
<@markup id="css">
	<@link rel="stylesheet" type="text/css" href="${page.url.context}/res/components/form/form.css"/>
    <@link rel="stylesheet" type="text/css" href="${page.url.context}/res/spg/css/jquery-ui.css"/>
    <@link rel="stylesheet" type="text/css" href="${page.url.context}/res/spg/css/style.css"/>
</@>
<#-- Widget creation -->
<@markup id="widgets">
		<@createWidgets group="dashlets"/>
</@>
<@markup id="html">
		<@uniqueIdDiv>
				<#assign el = args.htmlid?html>
				<#assign dashboardconfig=config.scoped['Dashboard']['dashboard']>
              <div class="dashlet">
                <script type="text/javascript">//<![CDATA[  
                    new SPG.dashlet.Reporting("${el}").setMessages(${messages}).setOptions({
                        workflow_definition_list: "${workflow_definition_list}" });           
                    new Alfresco.widget.DashletResizer("${el}");
                    new Alfresco.widget.DashletTitleBarActions("${el}").setOptions({
                        "actions": [{
                            "bubbleOnClick": {
                            "message": "${msg('dashlet.help')}"}, 
                            "cssClass": "help", 
                            "tooltip": "${msg('dashlet.help.tooltip')}"
                        }]
                    });
                //]]></script>
                    <div class="title">${msg("dashlet.name")} </div>
                    <div id="${el}-body" class="body myDashletbody">
                        <div id="${el}-global-container" class="myGContainer">
                          <label for="${el}-myInput" class="myInputLabel">${msg("worflow.choose")}</label>
                          <div id="${el}-alf-id2" class="yui-module yui-overlay absoluteOver">
                                <div class="bd">
                                    <div class="balloon">
                                        <div class="closeButton">x</div>
                                        <div class="text">${msg("Alfresco.forms.validation.invalid.message")}</div>
                                        <div class="balloon-arrow"></div>
                                    </div>
                                </div>
                          </div>
                          <div id="${el}-myAutoComplete">
                                <input id="${el}-myInput" type="text" placeholder='${msg("show.all.list")}' style="width: 227px" />
                                <div id="${el}-myContainer"></div>
                          </div>
                          <hr id="${el}-hr" class="hrDivider hidden" />
                          <div class="columnsContainer">
                            <div id="${el}-start-date" class="yui-gd  form-container form-fields hidden leftPicker">
                              <#assign controlId = el + "-start-cntrl">
                                <div class="yui-u first" style="width: 109px">
                                    <label for="${controlId}-date" class="myInputLabel">${msg("label.startdate")}</label>
                                </div>
                                <div class="yui-u relativeOver" style="float: left;">
                                    <script type="text/javascript">//<![CDATA[         
                                      (function()         
                                      {            
                                        Alfresco.constants.MM = new DatePicker2("${controlId}", "${el}-startd").setOptions(            
                                        {               
                                          currentValue: "",               
                                          showTime: false,               
                                          submitTime: false,               
                                          mandatory: true            
                                        }).setMessages(               
                                        ${messages}            
                                        );                        
                                      })();         
                                      //]]></script>    
                                    <input id="${el}-startd" type="hidden" name="startdate" value="" />
                                    <input id="${controlId}-date" name="-" type="text" class="date-entry" tabindex="0" placeholder='${msg("form.control.date-picker.display.date.format")}' />
                                    <a id="${controlId}-icon"><img src="${url.context}/res/components/form/images/calendar.png" class="datepicker-icon" tabindex="0" /></a>
                                    <div id="${controlId}" class="datepicker"></div>
                                    <div id="${controlId}-alf-id2" class="yui-module yui-overlay absoluteOver">
                                        <div class="bd">
                                            <div class="balloon">
                                                <div class="closeButton">x</div>
                                                <div class="text">${msg("Alfresco.forms.validation.invalid.message")}</div>
                                                <div class="balloon-arrow"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="${el}-end-date" class="form-field yui-gd  form-container form-fields hidden rightPicker">
                              <#assign controlId = el + "2-start-cntrl">
                                <div class="yui-u first" style="width: 109px">
                                    <label for="${controlId}-date" class="myInputLabel">${msg("label.enddate")}</label>
                                </div>
                                <div class="yui-u relativeOver" style="float: left;">
                                    <script type="text/javascript">//<![CDATA[         
                                      (function()         
                                      {            
                                        Alfresco.constants.MM = new DatePicker2("${controlId}", "${el}2-startd").setOptions(            
                                        {               
                                          currentValue: "",               
                                          showTime: false,               
                                          submitTime: false,               
                                          mandatory: true            
                                        }).setMessages(               
                                        ${messages}            
                                        );                        
                                      })();  
                                    //]]></script> 
                                    <input id="${el}2-startd" type="hidden" name="startdate" value="" />
                                    <input id="${controlId}-date" name="-" type="text" class="date-entry" tabindex="0" alf-validation-msg="" placeholder='${msg("form.control.date-picker.display.date.format")}' />
                                    <a id="${controlId}-icon"><img src="${url.context}/res/components/form/images/calendar.png" class="datepicker-icon" tabindex="0" /></a>
                                    <div id="${controlId}" class="datepicker"></div>
                                    <div id="${controlId}-alf-id2" class="yui-module yui-overlay absoluteOver">
                                        <div class="bd">
                                            <div class="balloon">
                                                <div class="closeButton">x</div>
                                                <div class="text">${msg("Alfresco.forms.validation.invalid.message")}</div>
                                                <div class="balloon-arrow"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </div>
                        </div>
                        <div id="${el}-basic"></div>
                        <div id="tabs" class="hidden">
                            <ul>
                                <li><a href="#tabs-1" style="font-size: 12px">${msg("courve.line")}</a></li>
                                <li><a href="#tabs-2" style="font-size: 12px">${msg("courve.Stacked.Column")}</a></li>
                                <button id="exportButton" title="${msg('save.text')}" class="exportButton" type="button" state="menu"> <img style="height: 16px;" alt="${msg('save.text')}" src="${page.url.context}/res/spg/img/export.png"> </button>
                            </ul>
                            <div id="tabs-1" class="tabsClass">
                                <canvas id="canvas" class="relativeOver"></canvas>
                            </div>
                            <div id="tabs-2" class="tabsClass">
                                <canvas id="canvas2" class="relativeOver"></canvas>
                            </div>
                        </div>
                    </div>
              </div>
              <div id="${el}-chartContainer"></div>
              <div id="${el}-dialog1" class="yui-pe-content" style="background-color:#FFFFFF;">
                  <div class="hd" style="padding: 8px 0px;">
                      <div id="${el}-titlePOPUP"></div>
                  </div>
                  <div id="${el}-bdPOPUP" class="bd popupTitle"> </div>
                  <div id="${el}-paginator" class="bd popupPaginator"> </div>
              </div>
              <script type="text/javascript">
                  var handleCancel = function() { 
                    myDialog.hide(); 
                  }; 
                  var myDialog = new YAHOO.widget.Dialog("${el}-dialog1", 
                  { 
                    
                    modal:true,
                    fixedcenter : true,
                    visible : false, 
                    draggable: false,
                    buttons : [ { text:"OK", handler: handleCancel , isDefault: true} ]
                  });
              </script>
		</@>
</@>